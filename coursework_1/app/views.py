from flask import render_template, flash
from app import app
from .forms import TaskForm

from app import db, models

@app.route('/')
def index():
	tasks = {}
	for querytask in models.Task.query.all():
		newTask = []
		newTask.append( querytask.task )
		newTask.append( querytask.completed )
		tasks[querytask.id] = newTask

	return render_template('index.html',
						   title='View All Tasks',
						   tasks=tasks)

@app.route('/completetask/<int:task_id>')
def completeTask(task_id):
	
	task = models.Task.query.filter_by(id=task_id).first()
	task.completed = True
	db.session.commit()

	tasks = {}
	for querytask in models.Task.query.all():
		newTask = []
		newTask.append( querytask.task )
		newTask.append( querytask.completed )
		tasks[querytask.id] = newTask

	return render_template('index.html',
						   title='View All Tasks',
						   tasks=tasks)	

@app.route('/completed')
def completed():
	tasks = {}
	for querytask in models.Task.query.all():
		newTask = []
		newTask.append( querytask.task )
		newTask.append( querytask.completed )
		tasks[querytask.id] = newTask

	return render_template('completed.html',
						   title='View Completed Tasks',
						   tasks=tasks)

@app.route('/create', methods=['GET', 'POST'])
def create():
	form = TaskForm()
	if form.validate_on_submit():
		print("Data = %s"%(form.task.data))

		task = models.Task(task=form.task.data, completed=False)
		db.session.add(task)
		db.session.commit()

	return render_template('create.html',
						   title='Create Task',
						   form=form)


'''
@app.route('/calculator', methods=['GET', 'POST'])
def calculator():
	form = CalculatorForm()
	if form.validate_on_submit():
		flash("Successfully received form data. %s + %s = %s"%(form.number1.data,form.number2.data, form.number1.data + form.number2.data))
	return render_template('calculator.html',
							title='Calculator',
							form=form)
'''