from flask_wtf import Form
from wtforms import IntegerField
from wtforms import TextField
from wtforms.validators import DataRequired

class TaskForm(Form):
	task = TextField("tasktext", validators=[DataRequired()])