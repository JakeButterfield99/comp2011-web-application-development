from app import db

class Task(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	task = db.Column(db.String(500), nullable=False)
	completed = db.Column(db.Boolean, nullable=False)